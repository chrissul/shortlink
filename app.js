//App Launcher File

//temporary global variable location
var port = 3000;
var LOGGING_STATUS = 'development';

var express = require('express');
var app = express();

//logging stuff
const winston = require('winston');
if(LOGGING_STATUS == 'production'){
	winston.remove(winston.transports.Console);
}
winston.add(winston.transports.File, { filename:'err.log' });

//dynamically create routes
var routes = [['/test','http://www.thexpcloud.com']];
app.get('/*', function (req,res){
	//get rid of stupid favico icon
	if(req.path == "/favicon.ico"){
	} else{
		//check the list to see if there
		//need a datbase to query the links
		if(req.path == routes[0][0]){
			res.redirect(routes[0][1]);
		} else {
			res.send("Not found");
		}
		res.end();
	}
});


app.get('*', function (req,res){
	res.send({'status':'Running'})
	res.end();
});


//app error handling stuff, has to come last lol
app.use(function(err, req, res, next) {
  // Do logging and user-friendly error message display
  winston.error('STACKTRACE: ' + err.stack);
  res.end();
})

app.listen(port, function () {
	console.log('Short Link Service started. Port: ' + port);
	//winston.info('SHORT LINK SERVICE STARTED. PORT:' + port);
});

//shutting down server stuff
process.on( 'SIGINT', function() {
  winston.info('SHORT LINK SERVICE STOPPED.')
  // some other closing procedures go heresi
  process.exit( );
})
